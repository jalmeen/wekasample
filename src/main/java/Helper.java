import weka.classifiers.Evaluation;
import weka.classifiers.trees.J48;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;
/**
 * Created by joy on 7/17/17.
 */
public class Helper {

    private Instances trainDataset;
    private Evaluation eval;
    private J48 tree;

    //method to load training dataset
    public void loadTrainingset() throws Exception {

        //load datasets
        ConverterUtils.DataSource source = new ConverterUtils.DataSource("/home/joy/Idea Projects/wekaSample/src/iris.arff");
        trainDataset = source.getDataSet();

        //set class index to the last attribute
        trainDataset.setClassIndex(trainDataset.numAttributes()-1);
    }

    //method for evaluation of model
    public void evaluateTestDataset() throws Exception {

        //create and build the classifier
        J48 tree = new J48();
        tree.buildClassifier(trainDataset);

        Evaluation eval = new Evaluation(trainDataset);

        //testing test dataset for evaluation
        ConverterUtils.DataSource source1 = new ConverterUtils.DataSource("/home/joy/Idea Projects/wekaSample/src/iris-test.arff");
        Instances testDataset = source1.getDataSet();

        testDataset.setClassIndex(testDataset.numAttributes() - 1);

        //now evaluate model
        eval.evaluateModel(tree, testDataset);

        System.out.println("Correct % = " + eval.pctCorrect());
        System.out.println("Incorrect % = " + eval.pctIncorrect());
        System.out.println("Error Rate = "+eval.errorRate());

        //the confusion matrix
        System.out.println(eval.toMatrixString("=== Overall Confusion Matrix ===\n"));
    }
}