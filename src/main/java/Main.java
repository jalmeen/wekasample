/**
 * Created by joy on 7/17/17.
 */

public class Main{

    public static void main(String args[]) throws Exception{

        Helper helper = new Helper();

        helper.loadTrainingset();
        helper.evaluateTestDataset();

    }
}